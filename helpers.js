
$(".link").click(function(e) {
    e.preventDefault();
    $('main div').fadeOut();
    $('#' + $(this).data('rel')).fadeIn();
});